'use strict'
const axios = require('axios');

const env = require('node-env-file');
const mongoose = require('mongoose');
const { Characters } = require('./models/index');
env(__dirname + '/.env');


const app = require('./app');


/* INICIALIZACION DE CONSTANTES */
const port = process.env.PORT;
const urlData = process.env.URL_DATA;


/** Por la naturaleza de los datos he optado por utilizar MongoDB */
mongoose.connect('mongodb://localhost:27017/got', { useNewUrlParser: true, useUnifiedTopology: true }).then(
    async () => {
        try{
            console.log('Conectado a Mongo!!!!')
            let charactersGot = await axios.get(urlData);
            let resp = await Characters.deleteMany({})
            
            if(resp.ok !== 1){
                console.log("Error en borrar data previa")
                return
            }
    
            resp =  await Characters.create(...charactersGot.data);
            if(resp.length == 0){
                console.log("Error al insertar data")
                return
            }
    
            app.listen(port, async function (err) {
                if (err) throw err;
                console.log('Servidor escuchando en http://localhost:4600');
                
            });
        } catch (exception){
            
            const { message, name } = exception;
            console.log(message, name);
        }
        
    },
    err => {
        console.log("Error en conexión a mongoDB:\n" + err);
    }
)


