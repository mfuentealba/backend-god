'use strict'

const express = require('express');
const characterController = require('../controllers/characterController');

const api = express.Router();



api.get('/characters/', characterController.getAllCharacters);
api.get('/characters/:id/', characterController.getCharacterId);

module.exports = api;