const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

var Schema = mongoose.Schema;

const CharactersSchema = Schema({
    titles: [
    String
],
spouse: [String],
children: [String],
allegiance: [
    String
],
books: [
    String
],
plod: Number,
longevity: [String],
plodB: Number,
plodC: Number,
longevityB: [String],
longevityC: [String],

name: String,
slug: String,
image: String,
gender: String,
culture: String,
house: String,
alive: Boolean,
createdAt: Date,
updatedAt: Date,
__v: Number,
pagerank: {
    title: String,
    rank: Number
},
id: String
})


CharactersSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Characters', CharactersSchema, 'Characters');