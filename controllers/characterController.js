'use strict'
const { Characters } = require('../models/index');





async function getAllCharacters(req, res) {
    try {
        const { page, opt, value } = req.query;
        let conf = { page: page, limit: 10 };
        let criteria = {};
        if(opt && value != ""){
            criteria[opt] = new RegExp(`^${value}`);
            
        }
        let charactersGot = await Characters.paginate(criteria, conf);
        res.send(JSON.stringify(charactersGot));
    } catch (exception) {

        const { message, name } = exception;
        console.log(message, name);
        res.send(JSON.stringify({ error: { message, name } }));
    }
}




async function getCharacterId(req, res) {
    try {
        let characterGot = await Characters.findOne({ id: req.params.id });
        
        res.send(JSON.stringify(characterGot));
    } catch (exception) {

        const { message, name } = exception;
        console.log(message, name);
        res.send(JSON.stringify({ error: { message, name } }));
    }






}



module.exports = {
    getAllCharacters,
    getCharacterId,


}